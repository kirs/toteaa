from fabric.api import *
import time

env.hosts = ["toteaa@django.iempire.ru:235"]

repo = "git@bitbucket.org:kirs/toteaa.git"
app_name = "toteaa"
app_root = "/var/www/" + app_name
current_path = app_root + "/current"


def host_type():
    run('uname -a')


def deploy():
    current_timestamp = str(int(time.time()))
    release_path = app_root + "/releases/" + current_timestamp
    run("git clone " + repo + " " + release_path)
    run("rm -rf " + current_path + " && ln -s " + release_path + " " + current_path)
    run("ln -s " + current_path + "/toteaa/static " + app_root + "/public_html/static")
    run("ln -s /usr/local/lib/python2.6/dist-packages/Django-1.3.1-py2.6.egg/django/contrib/admin/media " + app_root + "/public_html/static/admin")
    restart()


def restart():
    run("cd " + current_path + "/toteaa && python manage.py syncdb")
    socket = "/tmp/toteaa.sock"
    pid_file = "/var/www/toteaa/site.pid"
    run("kill -9 `cat " + pid_file + "`")
    run("cd " + current_path + "/toteaa && python manage.py runfcgi socket=" + socket + " method=prefork daemonize=true pidfile=" + pid_file + " errlog=/var/www/toteaa/err.log")
    run("chmod 777 " + socket)
