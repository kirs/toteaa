from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from djpjax import pjax
from toteaa.blog.models import *
import datetime


@pjax()
def post_view(request, post_id):
    current_date = datetime.datetime.now()

    try:
        post = Post.objects.get(id=post_id)
    except ValueError:
        raise Http404()

    comments_count = post.comment_set.count

    if request.method == 'POST':
        comment = Comment(post=post)
        comment.text = request.POST['text']
        comment.author = request.POST['author']
        comment.save()
        return HttpResponseRedirect('/posts/' + post_id)
    else:
        comment = Comment()

    # return render_to_response('post.html', locals())
    return TemplateResponse(request, "post.html", locals())


@pjax()
def posts_list(request):
    current_date = datetime.datetime.now()

    posts = Post.objects.all()
    return TemplateResponse(request, "posts.html", locals())


def delete_comment_view(request, comment_id):
    try:
        comment = Comment.objects.get(id=comment_id)
    except ValueError:
        raise Http404()
    post_id = comment.post.id
    comment.delete()
    return HttpResponseRedirect('/posts/' + str(post_id))
