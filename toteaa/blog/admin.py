from django.contrib import admin
from toteaa.blog.models import Post, Comment

admin.site.register(Post)
admin.site.register(Comment)

