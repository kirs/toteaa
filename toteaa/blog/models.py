from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=40)
    content = models.TextField()

    def __unicode__(self):
        return self.title

class Comment(models.Model):
    author = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    text = models.TextField()
    post = models.ForeignKey(Post)

    def __unicode__(self):
        return self.text + " (" + self.post.title + ")"
